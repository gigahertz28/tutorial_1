import sys, os

sys.path.insert(1, os.path.join(os.path.abspath('.'), 'C:\virtualenv\gigahertz-helloworld\Lib\site-packages'))

from flask import Flask
app = Flask(__name__)


@app.route("/")
def hello():
	return "Hello World!"
	
if __name__ == "__main__":
	app.run()